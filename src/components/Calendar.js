import _ from 'lodash';
import CalendarCell from './CalendarCell';
import service from '../service';

export default {
  name: 'Calendar',
  components: {
    CalendarCell
  },
  data() {
    return {
      hours: _.range(24),
      days: _.range(1, 32).map(day => 'Oct ' + day),
      isLoaded: false
    };
  },
  methods: {
    load() {
      this.isLoaded = true;
    },
    hide() {
      this.isLoaded = false;
    },
    searchAll() {
      service.searchAllCells();
    },
    dayHeaderClicked() {
      alert('dayHeaderClicked()');
    }
  },
  template: `
    <div class="calendar">
      <button class="btn" v-if="!isLoaded" @click="load">Load</button>
      <button class="btn" v-if="isLoaded" @click="hide">Hide</button>
      <button class="btn" v-if="isLoaded" @click="searchAll">Search all month</button>
      <table v-if="isLoaded">
        <tr>
          <th v-for="(day, key, index) in days" :key="index" class="day-header" @click="dayHeaderClicked(day)">
            {{day}}
          </th>
        </tr>
        <tr v-for="(hour, key, index) in hours" :key="index">
          <td v-for="(day, key, index) in days" :key="index" class="hour-cell">
            <CalendarCell :hour="hour" :day="day" />
          </td>
        </tr>
      </table>
    </div>
  `
};
